package BibliotecaApp.util;

import BibliotecaApp.model.Carte;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;

import static org.junit.Assert.*;

public class ValidatorTest {


    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void test_isStringOK_WhenValid() throws Exception {
     boolean valid = Validator.isStringOK("testingTest");
     assertTrue("Valid marked as invalid",valid);
    }

    @Test(expected = Exception.class)
    public void test_isStringOK_WhenNotValid() throws Exception {
        Validator.isStringOK("51255414554444");
    }

    @Test
    public void test_validateCarte_WhenValid() throws Exception {
        Carte carte = new Carte();
        carte.setTitlu("testingTest");
        carte.setAnAparitie("1970");
        carte.setAutori(Collections.singletonList("Tester McTester"));
        carte.setEditura("Humanitas");
        carte.setCuvinteCheie(Arrays.asList("testingTest","tester"));
        Validator.validateCarte(carte);
    }

    @Test
    public void test_validateCarte_WhenMissingListaAutori(){
        Carte carte = new Carte();
        carte.setTitlu("testingTest");
        carte.setAnAparitie("1970");
        carte.setAutori(null);
        carte.setEditura("Humanitas");
        carte.setCuvinteCheie(Arrays.asList("testingTest","tester"));
        Exception exception = null;
        try {
            Validator.validateCarte(carte);
        } catch (Exception e) {
            exception = e;
        }
        assertNotNull("Nu s-a aruncat exceptia",exception);
        assertEquals("Exceptia nu a avut mesajul asteptat!",
                "Lista autori vida!",exception.getMessage());
    }
}
