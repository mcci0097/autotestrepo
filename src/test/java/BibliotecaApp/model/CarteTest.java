package BibliotecaApp.model;

import org.junit.*;

import static org.junit.Assert.*;

public class CarteTest {

    private Carte c1, c2, c3;

    @BeforeClass
    public static void setUp() throws Exception {
        System.out.println("Before class Carte");
    }

    @Before
    public void setup() {
        c1 = new Carte();
        c2 = new Carte();
        c2.setTitlu("Praslea");
        c2.setAnAparitie("1999");
        System.out.println("setup");
    }

    @After
    public void teardown() {
        c1 = null;
        c2 = null;
    }

    @Test
    public void test_getTitlu() {
        assertEquals("Equal", "Praslea", c2.getTitlu());
        assertNotEquals("Not equal", "Praslea", c1.getTitlu());
        System.out.println("getTitlu succes");
    }

    @Test(expected = NullPointerException.class)
    public void test_getTitlu2() {
        System.out.println("Enetered getTitlu2");
        assertEquals("Equal", "Praslea", c3.getTitlu());
        System.out.println("getTitlu2 succes");
    }

    @AfterClass
    public static void tearDown() throws Exception {
        System.out.println("After class Carte");
    }

    @Test(timeout = 10)
    public void test_cicluInfinit() {
        int i = 0;
        while (i < i + 1)
            System.out.println(i++);
    }

    @Test
    public void getAnAparitie() {
        assertEquals("Equal", "1999", c2.getAnAparitie());
        assertNotEquals("Not equal", "1999", c1.getAnAparitie());
        System.out.println("getAnAparitie succes");
    }
}