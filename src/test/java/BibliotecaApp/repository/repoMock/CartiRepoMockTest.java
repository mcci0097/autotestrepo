package BibliotecaApp.repository.repoMock;

import BibliotecaApp.model.Carte;
import BibliotecaApp.util.Validator;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;

import static org.junit.Assert.*;

public class CartiRepoMockTest {
    private Carte c1;
    private Carte c2;
    private CartiRepoMock cartiRepoMock;
    @Before
    public void setUp() throws Exception {

        c1 = new Carte();
        c1.setTitlu("testingTest");
        c1.setAnAparitie("1970");
        c1.setAutori(Collections.singletonList("Tester McTester"));
        c1.setEditura("Humanitas");
        c1.setCuvinteCheie(Arrays.asList("testingTest","tester"));

        c2 = new Carte();
        c2.setTitlu("Test2");
        c2.setAnAparitie("1970");
        c2.setAutori(Collections.singletonList("Test 2"));
        c2.setEditura("Humanitas");
        c2.setCuvinteCheie(Arrays.asList("testingTest","tester"));

        cartiRepoMock = new CartiRepoMock();

    }

    @After
    public void tearDown() throws Exception {
        c1 = c2 = null;
        cartiRepoMock = null;

    }

    @Test
    public void cautaCarteInBibliotecaGoala() {
        assertEquals("biblioteca goala", 0, cartiRepoMock.cautaCarte("Test2").size());
        System.out.println("Biblioteca este goala");
    }

    @Test
    public void cautaCarteDupaAutorExistent() {
        cartiRepoMock.adaugaCarte(c1);
        cartiRepoMock.adaugaCarte(c2);

        assertEquals("carte cu un autor", 1, cartiRepoMock.cautaCarte("Test 2").size());
        System.out.println("Carte gasita");
    }

    @Test
    public void cautaCarteDupaAutorInexistent() {
        cartiRepoMock.adaugaCarte(c1);

        assertEquals("biblioteca plina", 0, cartiRepoMock.cautaCarte("Test2").size());
        System.out.println("Autor inexistent");
    }

}